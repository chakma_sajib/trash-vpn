import React, {useState} from 'react';
import {Picker} from 'react-native';
// import {Picker} from '@react-native-picker/picker';

export default function CountryPicker({countries}) {
  const [selectedLabel, setSelectedLabel] = useState('');

  const selectCountry = (value) => setSelectedLabel(value);

  return (
    <React.Fragment>
      <Picker
        selectedValue={selectedLabel}
        style={{height: 50}}
        onValueChange={selectCountry}>
        <Picker.Item label="Select a country" value="0" color="white" />
        {countries.length &&
          countries.map((country, index) => (
            <Picker.Item label={country} value={country} key={index} />
          ))}
      </Picker>
    </React.Fragment>
  );
}
