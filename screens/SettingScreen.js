import React from 'react';
import {Button, View, StyleSheet} from 'react-native';
import Animated from 'react-native-reanimated';
import {TapGestureHandler, State} from 'react-native-gesture-handler';
const {event, Value, cond, eq} = Animated;

export default function SettingScreen({navigation}) {
  const gestureState = new Value(-1);
  const _opactiy = cond(eq(gestureState, State.BEGAN), 0.2, 1);
  const onStateChange = event([
    {
      nativeEvent: {
        state: gestureState,
      },
    },
  ]);
  return (
    <View style={styles.container}>
      <Button
        title="Go to the home "
        onPress={() => navigation.navigate('Home')}
      />
      <TapGestureHandler onHandlerStateChange={onStateChange}>
        <Animated.View style={[styles.box, {opacity: _opactiy}]} />
      </TapGestureHandler>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    backgroundColor: 'tomato',
    width: 200,
    height: 200,
  },
});
