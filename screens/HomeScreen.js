import React, {useState} from 'react';
import {View, Text, Image, Switch, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {Flag} from 'react-native-svg-flagkit';
import {images, SIZES, COLORS} from '../constants';
import CountryPicker from '../components/CountryPicker';

export default function HomeScreen({navigation}) {
  const [country, setCountry] = useState(['Germany', 'Bangladesh', 'China']);
  const [toggle, setToggle] = useState(false);
  return (
    <View style={styles.container}>
      <View style={styles.homeScreenView}>
        <View style={styles.homeHeader}>
          <View style={styles.flagsContainer}>
            <Flag id={'US'} size={SIZES.flag} />
          </View>

          <View>
            <Icon.Button
              onPress={() => navigation.navigate('Setting')}
              name="setting"
              style={styles.setting}
            />
          </View>
        </View>

        <View style={styles.connectionNotifyContainer}>
          <Text style={styles.connectionMessage}>Disconnected</Text>
          <Image
            source={images.shield}
            resizeMode="contain"
            style={styles.image}
          />
        </View>
        <View style={styles.countryPickerContainer}>
          <CountryPicker countries={country} />
        </View>
      </View>

      <View
        style={{
          flex: 1,
          backgroundColor: COLORS.secondary,
        }}>
        <View style={styles.switchView}>
          <View>
            <Switch
              onValueChange={(value) => setToggle(value)}
              value={toggle}></Switch>
          </View>
        </View>

        <View style={styles.upgrade}>
          <Icon
            name="dashboard"
            size={SIZES.upgrade}
            style={styles.dashboardIcon}
          />
          <Text style={styles.upgrateContent}>Upgrade to Premium</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(10, 90, 237)',
  },
  homeScreenView: {
    flex: 2,
    marginTop: 40,
    padding: 10,
  },
  homeHeader: {
    flexDirection: 'row',
    color: 'white',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  flagsContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  setting: {
    color: 'white',
    fontSize: 30,
  },
  connectionNotifyContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 100,
  },
  connectionMessage: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  image: {
    width: 200,
    height: 200,
    marginTop: 20,
  },
  countryPickerContainer: {
    marginBottom: 20,
  },
  switchView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  upgrade: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20,
    alignItems: 'baseline',
  },
  dashboardIcon: {
    color: 'rgb(10, 90, 237)',
  },
  upgrateContent: {
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 20,
    color: 'rgb(10, 90, 237)',
  },
});
