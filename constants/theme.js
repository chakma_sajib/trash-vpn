import {Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

export const COLORS = {
  // Base colors
  primary: 'rgb(10, 90, 237)',
  secondary: 'white',

  // colors
  black: '#1E1B26',
  white: '#FFFFFF',
  lightGray: '#64676D',
  lightGray2: '#EFEFF0',
  lightGray3: '#D4D5D6',
  lightGray4: '#7D7E84',
  gray: '#2D3038',
  gray1: '#282C35',
  darkRed: '#31262F',
  lightRed: '#C5505E',
  darkBlue: '#22273B',
  lightBlue: '#424BAF',
  darkGreen: '#213432',
  lightGreen: '#31Ad66',
};

export const SIZES = {
  // Global sizes
  base: 8,
  font: 14,
  padding: 24,
  padding2: 36,

  // Flag icon size
  flag: 0.3,

  // Dashbord / upgrade icon size
  upgrade: 40,

  // Font sizes
  largeTitle: 50,
  h1: 30,
  h2: 22,
  h3: 16,
  h4: 14,
  body1: 30,
  body2: 20,
  body3: 16,
  body4: 14,

  // app dimensions
  width,
  height,
};

const appTheme = {COLORS, SIZES};
export default appTheme;
